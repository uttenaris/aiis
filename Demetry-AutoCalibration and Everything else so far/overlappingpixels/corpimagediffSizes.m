% cropped miages fo dif sizes
% I change slightly the code so that now it can work on cropped images with different sizes. Timing for large images (2009A/Vista64 bits/Nvidia FX 4800):
% - With GPU computing: Elapsed time is 0.518769 seconds.
% - Without GPU: Elapsed time is 1.996390 seconds.

% Bruno

%%
function corpimagediffSizes(GPU)

%% Test data
fullimg = peaks(1000);

% Shift between two images (actually of their upper/right corners)
dx = -13; dy = 27;

% Generate cropped images
idx1 = 50:960;
idy1 = 35:849;

idx2 = idx1(1)+dx:640;
idy2 = idy1(1)+dy:900;

% Crop
A1 = fullimg(idy1,idx1);
A2 = fullimg(idy2,idx2);
% clean up
clear fullimg dx dy idx1 idx2 idy1 idy2

% Use GPU flag
if nargin<1
    GPU = true;
end

%%
tic
% should return the same input as above
[dx, dy] = pmatch(A1, A2, [], GPU);
toc
fprintf('Shift found [pixel] is (%d,%d)\n', dy, dx);


%%
% Graphic check

fig=figure(1);
clf(fig);
ax = axes('Parent',fig);
x1 = 1:size(A1,2);
y1 = 1:size(A1,1);
hold(ax,'on');
imagesc(x1,y1,A1,'Parent',ax);
plot3(ax,x1([1 end end 1 1])+[-1 1 1 -1 -1]/2,...
         y1([1 1 end end 1])+[-1 -1 1 1 -1]/2,...
         ones(1,5),'k');
x2 = dx + (1:size(A2,2));
y2 = dy + (1:size(A2,1));
imagesc(x2,y2,A2,'Parent',ax);
plot3(ax,x2([1 end end 1 1])+[-1 1 1 -1 -1]/2,...
         y2([1 1 end end 1])+[-1 -1 1 1 -1]/2,...
          ones(1,5),'k');
% Intersection
x = intersect(x1,x2);
y = intersect(y1,y2);
plot3(ax,x([1 end end 1 1])+[-1 1 1 -1 -1]/2,...
         y([1 1 end end 1])+[-1 -1 1 1 -1]/2,...
          ones(1,5),'r','LineWidth',3);
      
end % PatternMatching

%%
function [dx, dy] = pmatch(A1, A2, maxshift, GPU)
% function [dx dy] = pmatch(A1, A2, maxshift, GPU)
% Pattern matching engine

% Use GPU
if nargin<3 || isempty(maxshift)
    % We don't look for shift larger than this (see ImageAnalyst's post)
    % half of the size of A1 and A2
    maxshift = ceil(0.5*min(size(A1),size(A2)));
end

% Use GPU
if nargin<4 || isempty(GPU)
    GPU = true;
end

if isscalar(maxshift)
    % common margin duplicated for both dimensions
    maxshift = maxshift([1 1]);
end

% Select 2D convolution engine
if ~isempty(which('convnfft'))
    % http://www.mathworks.com/matlabcentral/fileexchange/24504
    convfun = @convnfft;
    convarg = {[], GPU};
    fprintf('GPU/jacket flag = %i\n', GPU);
else
    % This one will last almost forever
    convfun = @conv2;
    convarg = {};
    fprintf('Slow Matlab CONV2...\n');
end

% Correlation engine
A2f = A2(end:-1:1,end:-1:1);
C = convfun(A1, A2f, 'full', convarg{:});
V1 = convfun(A1.^2, ones(size(A2f)), 'full', convarg{:});
V2 = convfun(ones(size(A1)), A2f.^2, 'full', convarg{:});
C2 = C.^2 ./ (V1.*V2);
center = size(A2f);
C2 = C2(center(1)+(-maxshift(1):maxshift(2)), ...
        center(2)+(-maxshift(2):maxshift(2)));
[cmax, ilin] = max(C2(:));
[iy, ix] = ind2sub(size(C2),ilin);
dx = ix - (maxshift(2)+1);
dy = iy - (maxshift(1)+1);

end % pmatch 