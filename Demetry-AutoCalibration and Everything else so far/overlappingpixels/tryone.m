% method 1
%% Data
fullimg = peaks(50);

% Shift between two images
dx = 3; dy = 7;

% Generate crop indexes
idx1 = 10:40;
idy1 = 10:39;
idx2 = idx1+dx;
idy2 = idy1+dy;

% Crop
A1 = fullimg(idy1,idx1);
A2 = fullimg(idy2,idx2);
clear fullimg dx dy idx1 idx2 idy1 idy2 % cleanup

%%
% Engine

% We don't look for shift larger than this (see ImageAnalyst's post)
% To prevent meaningless small overlap detection
maxshift = 20;

% Note: There is in FEX a faster CONV2 function for large images
A2f = A2(end:-1:1,end:-1:1); % flip
C = conv2(A1, A2f, 'full');
V1 = conv2(A1.^2, ones(size(A2f)), 'full');
V2 = conv2(ones(size(A1)), A2f.^2, 'full');
C2 = C.^2 ./ (V1.*V2);
center = (size(C2)+1)/2;
C2 = C2(center(1)+(-maxshift:maxshift), ...
        center(2)+(-maxshift:maxshift));
[cmax ilin] = max(C2(:));
[iy ix] = ind2sub(size(C2),ilin);
dx = ix - (maxshift+1);
dy = iy - (maxshift+1);

%%
% Check
fprintf('Shift found [pixel] is (%d,%d)\n', dy,dx);

figure
x1 = 1:size(A1,2);
y1 = 1:size(A1,1);
hold on
imagesc(x1,y1,A1);
imagesc(x1+dx,y1+dy,A2);

% Bruno 