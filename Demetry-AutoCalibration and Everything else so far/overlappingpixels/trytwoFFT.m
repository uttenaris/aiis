% faster FFT implementation
% In fact I strongly recommend using FFT-based convolution. It's about 100 time faster when matching two 500x500 images.

%% Data
fullimg = peaks(500);

% Shift between two images
dx = -13; dy = 27;

% Generate crops
idx1 = 20:460;
idy1 = 15:449;

idx2 = idx1+dx;
idy2 = idy1+dy;

% Crop
% A1 = fullimg(idy1,idx1);
% A2 = fullimg(idy2,idx2);

%%Read in Image
tryone = imread('A1 (1).jpg');

if size(tryone,3)==3 
    a1 = rgb2gray(tryone);
    I = imadjust(a1);
    imshow(I);
else
    I = imadjust(tryone);
end
A1 = single(I);

tryone = imread('A1 (2).jpg');

if size(tryone,3)==3 
    a1 = rgb2gray(tryone);
    I = imadjust(a1);
    imshow(I);
else
    I = imadjust(tryone);
end

A2 = single(I);

% A1 = 
% A2 =

clear fullimg dx dy idx1 idx2 idy1 idy2

%%
% Engine

% We don't look for shift larger than this (see ImageAnalyst's post)
maxshift = 10;

tic
% Select 2D convolution engine
% if ~isempty(which('convnfft'))
%     % http://www.mathworks.com/matlabcentral/fileexchange/24504
%     convfun = @convnfft;
% else
%     % This one will last almost forever
%     convfun = @conv2;
% end
convfun = @convnfft;



% Input = [alpha1; alpha1; alpha2; alpha2; beta1; beta1; beta2; beta2];
% Method = {'FresnelC'; FresnelS'; 'FresnelC'; FresnelS'; 'FresnelC'; FresnelS'; 'FresnelC'; FresnelS'};
% 
% % Open matlabpool. Requires 8 workers
% 
% matlabpool open 8
% % Perform all 8 calculation together
% 
% spmd
% 	%%% mfun cannot be a nested function, has to be an m file %%%
% 	Results = mfun(Method{labindex}, Input(labindex));
% end
% 
% % Export results. Assume mun output is a scalar
% Results = [Results{:}];
% matlabpool close;




A2f = A2(end:-1:1,end:-1:1);

matlabpool open 2
% parpool('local',2);
spmd
%    Input1 = [A1.^2; ones(size(A1))];
%    Input2 = [ones(size(A2f)); A2f.^2];
%    Results = convfun(Input1(labindex), Input2(labindex),'full');
   Input1 = [A1.^2; ones(size(A1))];
   Input2 = [ones(size(A2f)); A2f.^2];
   Results = convfun(Input1(labindex), Input2(labindex),'full');

end


C = convfun(A1, A2f, 'full');
V1 = convfun(A1.^2, ones(size(A2f)), 'full');
V2 = convfun(ones(size(A1)), A2f.^2, 'full');

Results = [Results{:}];
disp 
matlabpool close;
% parpool close

V1 = Results(0);
V2 = Results(1);




C2 = C.^2 ./ (V1.*V2);
center = (size(C2)+1)/2;
C2 = C2(center(1)+(-maxshift:maxshift), ...
        center(2)+(-maxshift:maxshift));
[cmax, ilin] = max(C2(:));
[iy, ix] = ind2sub(size(C2),ilin);
dx = ix - (maxshift+1);
dy = iy - (maxshift+1);
toc

%%
% Check
fprintf('Shift found [pixel] is (%d,%d)\n', dy,dx);

figure(1);
clf
x1 = 1:size(A1,2);
y1 = 1:size(A1,1);
hold on
imagesc(x1,y1,A1);
plot3(x1([1 end end 1 1])+[-1 1 1 -1 -1]/2,...
      y1([1 1 end end 1])+[-1 -1 1 1 -1]/2,...
      ones(1,5),'k');
x2 = x1+dx;
y2 = y1+dy;
imagesc(x2,y2,A2);
plot3(x2([1 end end 1 1])+[-1 1 1 -1 -1]/2,...
      y2([1 1 end end 1])+[-1 -1 1 1 -1]/2,...
      ones(1,5),'k');
  
% Bruno 